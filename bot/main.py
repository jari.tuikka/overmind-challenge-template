import json
from pathlib import Path

import sc2
from sc2 import Race, Difficulty
from sc2.constants import *
from sc2.player import Bot, Computer

# Bots are created as classes and they need to have on_step method defined.
# Do not change the name of the class!
from bot.army import Army
from bot.building import Building
from bot.combat import Combat


class MyBot(sc2.BotAI):
    with open(Path(__file__).parent / "../botinfo.json") as f:
        NAME = json.load(f)["name"]

    def __init__(self):
        self.actions = []
        self.building = Building(self)
        self.army = Army(self)
        self.combat = Combat(self)

        self.buildProbes = True

    # def _prepare_first_step(self):
    #     self.expansion_locations
    #     return super()._prepare_first_step()

    # On_step method is invoked each game-tick and should not take more than
    # 2 seconds to run, otherwise the bot will timeout and cannot receive new
    # orders.
    # It is important to note that on_step is asynchronous - meaning practices
    # for asynchronous programming should be followed.
    async def on_step(self, iteration):
        await self.distribute_workers()

        if self.buildProbes:
            await self.building.build_workers()

        if self.workers.amount <= 15 and self.time < 20.0:
            self.buildProbes = False
        else:
            self.buildProbes = True

        await self.building.build_pylons()

        if self.time == 32.0:
            await self.building.doChronoBoostForNexus()

        if self.units(GATEWAY).amount < 1:
            await self.building.build_gateway()

        if self.units(GATEWAY).amount >= 1:
            await self.building.build_assimilator()

#        if self.units(GATEWAY).amount < 4:
#            await self.building.build_gateway()

        if iteration == 0:
            await self.chat_send(f"Name: {self.NAME}")
            # FIXME: uncomment to have simplest winning strategy!
            actions = []

            # for worker in self.workers:
            #    actions.append(worker.attack(self.enemy_start_locations[0]))
            # await self.do_actions(actions)

        # if not self.units(NEXUS).ready.exists:
        #     for worker in self.workers:
        #         self.actions.append(worker.attack(self.enemy_start_locations[0]))
        #         await self.do_actions(self.actions)
        #     return
