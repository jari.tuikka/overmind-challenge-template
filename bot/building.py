import sc2
from sc2 import Race, Difficulty
from sc2.constants import *
from sc2.player import Bot, Computer


class Building(sc2.BotAI):
    def __init__(self, state):
        self.sulf = state

    async def build_workers(self):
        for nexus in self.sulf.units(NEXUS).ready.noqueue:
            if self.sulf.can_afford(PROBE):
                await self.sulf.do(nexus.train(PROBE))

    async def build_pylons(self):
        if self.sulf.supply_left < 5 and not self.sulf.already_pending(PYLON):
            nexuses = self.sulf.units(NEXUS).ready
            if nexuses.exists:
                if self.sulf.can_afford(PYLON):
                    await self.sulf.build(PYLON, near=nexuses.first)

    async def build_assimilator(self):
        for nexus in self.sulf.units(NEXUS).ready:
            vespenes = self.sulf.state.vespene_geyser.closer_than(25.0, nexus)
            for vespene in vespenes:
                if not self.sulf.can_afford(ASSIMILATOR):
                    break
                worker = self.sulf.select_build_worker(vespene.position)
                if worker is None:
                    break
                if not self.sulf.units(ASSIMILATOR).closer_than(1.0, vespene).exists:
                    await self.sulf.do(worker.build(ASSIMILATOR, vespene))

    async def build_gateway(self):
        nexuses = self.sulf.units(NEXUS).ready
        if nexuses.exists:
            if self.sulf.can_afford(GATEWAY):
                await self.sulf.build(GATEWAY, near=nexuses.first)

    # TODO
    async def chrono_for_target(self, target):
        nexuses = self.sulf.units(NEXUS).ready
        for nexus in nexuses:
            if not nexus.has_buff(BuffId.CHRONOBOOSTENERGYCOST):
                abilities = await self.get_available_abilities(nexus)
            if self.AbilityId.EFFECT_CHRONOBOOSTENERGYCOST in abilities:
                self.sulf.actions.append(nexus(AbilityId.EFFECT_CHRONOBOOSTENERGYCOST, target))

